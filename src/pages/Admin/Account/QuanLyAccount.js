import React, { Fragment, useEffect } from 'react'
import { Input, Popconfirm, Table } from 'antd';
import { history } from './../../../App';
import { _account, _add, _admin, _edit } from '../../../utils/Utils/ConfigPath';
import { useDispatch, useSelector } from 'react-redux';
import { DeleteUserAction, GetListUserAction } from '../../../redux/Actions/QuanLyAccountAction';
import { BsFillTrashFill, BsPencilSquare } from 'react-icons/bs';



const { Search } = Input;

export default function QuanLyAccount() {
    const dispatch = useDispatch();
    const { lstUser } = useSelector(state => state.QuanLyAccountReducer);
    const { userLogin } = useSelector(state => state.QuanLyAccountReducer);
    let role = userLogin?.account.RoleType.RoleType
    useEffect(() => {
        dispatch(GetListUserAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const onSearch = (value) => {
        dispatch(GetListUserAction(value))
    };

    const cancel = (e) => {
        console.log(e);
    };


    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
        },
        {
            title: 'Email',
            dataIndex: 'Email'
        },
        {
            title: 'Họ và Tên',
            dataIndex: 'Username'
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'PhoneNumber',
            render: (text, user) => {
                return <Fragment>
                    {user.PhoneNumber === null ?
                        <div className='text-yellow-500 flex text-base italic'>
                            <span>Chưa có số điện thoại...</span>
                        </div>
                        : <span>{user.PhoneNumber}</span>}
                </Fragment>
            },
        },
        {
            title: 'Địa chỉ',
            dataIndex: 'Address',
            render: (text, user) => {
                return <Fragment>
                    {user.Address === null ?
                        <div className='text-yellow-500 flex text-base italic'>
                            <span>Chưa có địa chỉ...</span>
                        </div>
                        : <span>{user.Address}</span>}
                </Fragment>
            },
        },
        {
            title: 'Quyền',
            render: (text, user) => {
                return <Fragment>
                    {user.roleType.RoleType === 'CLIENT' ? <span>{user.roleType.RoleType}</span> : <>{user.roleType.RoleType === 'ADMIN' ? <span className='text-red-500'>{user.roleType.RoleType}</span> : <span className='text-red-500 font-bold'>{user.roleType.RoleType}</span>}</>}
                </Fragment>
            }
        },
        {
            title: 'Điểm',
            render: (text, user) => {
                // console.log('user', user)
                return <Fragment>
                    {user.roleType.RoleType === 'CLIENT' ? <span>{user.Point}</span> : ''}
                </Fragment>
            }
        },
        {
            title: 'Xếp hạng',
            render: (text, user) => {
                return <Fragment>
                    {user.roleType.RoleType === 'CLIENT' ? <span>{user?.rankType?.PointName}</span> : ''}
                </Fragment>
            }
        },
        {
            title: '',
            dataIndex: 'id',
            render: (text, user) => {
                return <>
                    {user.roleType.RoleType === 'CLIENT' ? <div className='text-center -ml-5'>
                        <button className='text-red-500 hover:text-red-900' title='Xóa' >
                            <Popconfirm
                                title="Bạn có muốn xóa tài khoản không?"
                                onConfirm={() => { dispatch(DeleteUserAction(user.id)) }}
                                onCancel={cancel}
                                okText="Có"
                                cancelText="Không"
                            >
                                <BsFillTrashFill style={{ fontSize: 25 }} />
                            </Popconfirm>
                        </button>
                    </div> : <>
                        {role === 'ADMIN' ? <>
                            {user.roleType.RoleType === 'SUPER_ADMIN' ? <div className='text-center -ml-5'>
                                <button className='text-red-500 hover:text-red-900' title='Xóa' >
                                    <Popconfirm
                                        title="Bạn có muốn xóa tài khoản không?"
                                        onConfirm={() => { dispatch(DeleteUserAction(user.id)) }}
                                        onCancel={cancel}
                                        okText="Có"
                                        cancelText="Không"
                                    >
                                        <BsFillTrashFill style={{ fontSize: 25 }} />
                                    </Popconfirm>
                                </button>
                            </div> : <div className='flex'>
                                <button className='mx-4 text-green-500 hover:text-green-900' title='Sửa' onClick={() => {
                                    history.push(`${_admin}${_account}${_edit}/${user.id}`)
                                }}>
                                    <BsPencilSquare style={{ fontSize: 25 }} />
                                </button>
                                <button className='mx-4 text-red-500 hover:text-red-900 content-end' title='Xóa' >
                                    <Popconfirm
                                        title="Bạn có muốn xóa tài khoản không?"
                                        onConfirm={() => { dispatch(DeleteUserAction(user.id)) }}
                                        onCancel={cancel}
                                        okText="Có"
                                        cancelText="Không"
                                    >
                                        <BsFillTrashFill style={{ fontSize: 25 }} />
                                    </Popconfirm>
                                </button>
                            </div>}
                        </> : <div className='flex'>
                            <button className='mx-4 text-green-500 hover:text-green-900' title='Sửa' onClick={() => {
                                history.push(`${_admin}${_account}${_edit}/${user.id}`)
                            }}>
                                <BsPencilSquare style={{ fontSize: 25 }} />
                            </button>
                            <button className='mx-4 text-red-500 hover:text-red-900 content-end' title='Xóa' >
                                <Popconfirm
                                    title="Bạn có muốn xóa tài khoản không?"
                                    onConfirm={() => { dispatch(DeleteUserAction(user.id)) }}
                                    onCancel={cancel}
                                    okText="Có"
                                    cancelText="Không"
                                >
                                    <BsFillTrashFill style={{ fontSize: 25 }} />
                                </Popconfirm>
                            </button>
                        </div>}
                    </>
                    }

                </>
            },
        },
    ]
    return (
        <Fragment>
            <div className='container mt-4'>
                <div className='flex justify-center'>
                    <h2 className='text-4xl font-bold text-yellow-500'>Quản lý Tài khoản</h2>
                </div>
                <div className='my-10 flex justify-between'>
                    {role === 'SUPER_ADMIN' ? <button type='button' className='border-2 border-yellow-600 rounded w-24 h-10 text-lg font-bold text-yellow-500 hover:text-white hover:bg-yellow-600' onClick={() => {
                        history.push(`${_admin}${_account}${_add}`)
                    }}>Thêm </button> : <button type='button' className='border-2 border-yellow-600 rounded w-24 h-10 text-lg font-bold text-yellow-500 cursor-not-allowed' title='Bạn không có quyền thực hiện thao tác này!' disabled onClick={() => {
                        history.push(`${_admin}${_account}${_add}`)
                    }}>Thêm </button>}
                    <div className='w-1/2'>
                        <Search size='large' placeholder="Bạn muốn tìm gì?..." onSearch={onSearch} enterButton />
                    </div>
                </div>
                <Table dataSource={lstUser} columns={columns} rowKey='id' />
            </div>
        </Fragment>
    )
}
