import { useFormik } from 'formik';
import * as Yup from "yup";
import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { UpdateAccountAction } from '../../../redux/Actions/QuanLyAccountAction';

export default function UpdateAccount() {
    const dispatch = useDispatch();


    const { userLogin } = useSelector(state => state.QuanLyAccountReducer);
    console.log('first', userLogin)
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            Email: userLogin.account?.Email,
            Username: userLogin.account?.Username,
            Password: '',
            RoleType: 2,
            PhoneNumber: userLogin.account?.PhoneNumber,
            Address: userLogin.account?.Address
        },
        validationSchema: Yup.object({
            Email: Yup.string()
                .email('Email không đúng!'),
            Username: Yup.string()
                .required("Không được trống !"),
            Password: Yup.string()
                .min(6, "Tối thiểu 6 kí tự")
                .required("Không được trống !"),

            PhoneNumber: Yup.string()
                .matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/, {
                    message: "Số điện thoại chưa đúng",
                    excludeEmptyString: false,
                })
                .required("Không được trống !"),
            Address: Yup.string()
                .required("Không được trống !"),
        }),
        onSubmit: values => {
            dispatch(UpdateAccountAction(userLogin.account.id, values))
        }
    })
    return (
        <div className='grid grid-cols-12 mt-56'>
            <div className='col-start-3 col-span-9'>
                <div className='grid grid-rows mx-8'>
                    <span className='uppercase text-2xl text-yellow-500 font-bold font-serif'>Thông tin tài khoản</span>
                    <div className='p-4 w-3/4'>
                        <form onSubmit={formik.handleSubmit}>
                            <div className='mb-2'>Email:</div>
                            <input type="text" name='Email' onChange={formik.handleChange} value={formik.values?.Email} className='p-3 border-gray border rounded-lg focus:outline-none focus:border-red-500 focus:ring-1 focus:ring-red-500 w-full' />
                            {formik.errors.Email && formik.touched.Email && (
                                <p className='m-0 mt-1 text-red-600'>{formik.errors.Email}</p>
                            )}
                            <div className='mt-4 mb-2'>Số điện thoại:</div>
                            <input type="text" name='PhoneNumber' onChange={formik.handleChange} value={formik.values?.PhoneNumber} className='p-3 border-gray border rounded-lg focus:outline-none focus:border-red-500 focus:ring-1 focus:ring-red-500 w-full' placeholder="Nhập số điện thoại..." />
                            {formik.errors.PhoneNumber && formik.touched.PhoneNumber && (
                                <p className='m-0 mt-1 text-red-600'>{formik.errors.PhoneNumber}</p>
                            )}
                            <div className='mb-2'>Họ và Tên:</div>
                            <input type="text" name='Username' onChange={formik.handleChange} value={formik.values?.Username} className='p-3 border-gray border rounded-lg focus:outline-none focus:border-red-500 focus:ring-1 focus:ring-red-500 w-full' />
                            {formik.errors.Username && formik.touched.Username && (
                                <p className='m-0 mt-1 text-red-600'>{formik.errors.Username}</p>
                            )}
                            <div className='mt-4 mb-2'>Địa chỉ:</div>
                            <input type="text" name='Address' onChange={formik.handleChange} value={formik.values?.Address} className='p-3 border-gray border rounded-lg focus:outline-none focus:border-red-500 focus:ring-1 focus:ring-red-500 w-full' placeholder="Nhập địa chỉ..." />
                            {formik.errors.Address && formik.touched.Address && (
                                <p className='m-0 mt-1 text-red-600'>{formik.errors.Address}</p>
                            )}
                            <h1 className='mt-4 uppercase text-2xl font-bold'>Thay đổi mật khẩu</h1>
                            <span className='mt-4 italic text-base'>Để đảm bảo tính bảo mật vui lòng đặt mật khẩu trên 6 ký tự!</span>
                            <div className='mt-4 mb-2'>Mật khẩu mới:</div>
                            <input type="text" name='Password' onChange={formik.handleChange} className='p-3 border-gray border rounded-lg focus:outline-none focus:border-red-500 focus:ring-1 focus:ring-red-500 w-full' placeholder="Nhập mật khẩu mới..." />
                            {formik.errors.Password && formik.touched.Password && (
                                <p className='m-0 mt-1 text-red-600'>{formik.errors.Password}</p>
                            )}
                            <div className='text-end mt-16'>
                                <button type='submit' className='px-4 py-2 border rounded text-lg font-bold hover:text-white hover:bg-yellow-500' >Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    )
}
