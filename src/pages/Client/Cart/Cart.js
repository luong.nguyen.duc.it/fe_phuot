import React, { Fragment, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { AiOutlineMinus, AiOutlinePlus } from 'react-icons/ai';
import { IMG } from '../../../utils/Settings/config';
import { DELETE_CART, UPDATE_CART } from '../../../redux/Types/QuanLyCartType';
import { CheckoutAction, RequirementCheckoutAction } from '../../../redux/Actions/QuanLyCheckoutAction';
import { POINT, USER_LOGIN } from './../../../redux/Types/QuanLyAccountType';
import { GetListUserAction, UpdateUserPointAction } from '../../../redux/Actions/QuanLyAccountAction';
import { history } from '../../../App';

export default function Cart() {
    const [parsedCart, setParsedCart] = useState(null);
    const dispatch = useDispatch();
    const { lstUser } = useSelector(state => state.QuanLyAccountReducer);
    const { userLogin } = useSelector(state => state.QuanLyAccountReducer);
    useEffect(() => {
        dispatch(GetListUserAction())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    // console.log('lstUser', lstUser)
    // console.log('userLogin', userLogin)
    let discountUser = 0
    let rank = ''
    lstUser.map((item, index) => {
        if (item.id === userLogin?.account?.id) {
            rank = item.rankType.PointName
            discountUser = item.rankType.Discount
        }
    })


    const { cart } = useSelector(state => state.QuanLyCartReducer);
    const cartFromSession = sessionStorage.getItem('cart');

    useEffect(() => {

        if (cartFromSession) {
            const parsedCart = JSON.parse(cartFromSession);
            setParsedCart(parsedCart);
        }
    }, [cartFromSession])
    let totalAll = 0
    let total = 0;
    parsedCart?.forEach(item => {
        total += (item.Price - (item.Price * (item.Discount / 100))) * item.Quantity
    });
    // console.log('total', Math.floor(total * 0.0001))
    totalAll = Math.floor(total - total * discountUser / 100)
    // console.log('totalAll', totalAll)


    const renderCart = () => {
        return parsedCart?.map((item, index) => {
            // console.log('item', item)
            return <tr key={index}>
                <th className='grid grid-cols-4 mt-6'>
                    <img src={`${IMG}${item.ProductImage}`} alt={item.ProductName} />
                    <div className='col-span-3 pl-4 text-start'>
                        <div className='text-xl text-red-600'>{item.ProductName}</div>
                        <div className='font-normal my-2'>{item.Description}</div>
                        <button type='button' onClick={() => {
                            dispatch({
                                type: DELETE_CART,
                                data: {
                                    id: item.Product_ID
                                }
                            })

                        }} className='text-red-600 hover:text-red-700'>Xóa</button>
                    </div>
                </th>
                <th className='text-lg text-red-500'>
                    {item.Discount} %
                </th>
                <th className='text-xl'>
                    {(item.Price * 1).toLocaleString()} đ
                </th>
                <th style={{ width: 150 }}>
                    <div className='h-10 flex justify-center'>
                        <button id='decrease' type='button' onClick={(e) => {
                            if (item.Quantity > 1) {
                                dispatch({
                                    type: UPDATE_CART,
                                    data: {
                                        soLuong: -1,
                                        id: item.Product_ID
                                    }
                                })
                            }


                        }} className={`border-2 text-lg px-1  ${item.Quantity === 1 ? 'inactive cursor-not-allowed' : 'hover:text-red-500 hover:border-red-500'}`} disabled={item.Quantity === 1}><AiOutlineMinus /></button>
                        <input type="text" disabled value={item.Quantity} id='soluong' name="number" className="text-center w-1/4 p-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-red-500 focus:ring-red-500 block sm:text-sm focus:ring-1" />
                        <button type='button' onClick={() => {
                            dispatch({
                                type: UPDATE_CART,
                                data: {
                                    soLuong: 1,
                                    id: item.Product_ID
                                }
                            })
                        }} className='border-2 text-lg px-1 hover:text-red-500 hover:border-red-500'><AiOutlinePlus /></button>
                    </div>
                </th>
                <th className='text-xl'>
                    {((item.Price - (item.Price * (item.Discount / 100))) * item.Quantity).toLocaleString()}đ
                </th>
            </tr>
        })
    }


    return (
        <Fragment>
            <div className='grid grid-cols-5 my-48'>
                <div className='col-start-2 col-span-3'>
                    <div className='py-8 border-b-2 uppercase text-center text-3xl text-yellow-500 font-bold'>Giỏ hàng</div>
                    <div className='mt-4'>
                        <table className='w-full'>
                            <thead>
                                <tr className='border-y-2'>
                                    <th className='w-3/5 py-6'>Sản phẩm</th>
                                    <th>Giảm giá</th>
                                    <th>Đơn giá</th>
                                    <th>Số lượng</th>
                                    <th>Tổng giá</th>
                                </tr>
                            </thead>
                            <tbody>
                                {renderCart()}

                            </tbody>
                        </table>
                    </div>
                    <div className='border-t-2 mt-16'>
                        <div className='grid grid-cols-12 pt-6'>
                            <div className='col-span-8'>
                            </div>
                            <div className='col-span-4 text-end'>
                                <div>Tạm tính <span className='text-2xl font-medium text-red-600 mx-2'>{total.toLocaleString()} đ</span></div>
                                <div className='mb-2'>Bởi vì bạn có Rank: <span className='text-2xl font-medium text-yellow-600'>{rank}</span> bạn sẽ được giảm giá: <span className='text-lg text-red-500'>{discountUser} %</span></div>
                                <div>Tổng <span className='text-3xl font-bold text-red-600 mx-2'>{totalAll.toLocaleString()} đ</span></div>

                                <div>
                                    <button type='button' onClick={async () => {
                                        dispatch(UpdateUserPointAction(userLogin?.account?.id, Math.floor(totalAll * 0.0001)))
                                        dispatch(CheckoutAction(parsedCart))


                                        if (
                                            !JSON.parse(sessionStorage.getItem(USER_LOGIN))?.account
                                                ?.id
                                        ) {
                                            alert("Bạn cần đăng nhập trước khi thanh toán");
                                            history.push("/login");
                                            return;
                                        }

                                        let data = [];
                                        // let obj = {};
                                        for (let index = 0; index < parsedCart?.length; index++) {
                                            let obj = {};
                                            obj.name = parsedCart[index]?.ProductName;
                                            obj.sku = "product";
                                            obj.price = ((parsedCart[index].Price - parsedCart[index].Price * (parsedCart[index].Discount) / 100) - (parsedCart[index].Price - parsedCart[index].Price * (parsedCart[index].Discount) / 100) * discountUser / 100).toString();
                                            obj.currency = "USD";
                                            obj.quantity = "1";
                                            data.push(obj);
                                        }

                                        const payload = {
                                            data,
                                            sum: totalAll,
                                        };




                                        await dispatch(RequirementCheckoutAction(payload));


                                        // sessionStorage.removeItem('cart');
                                    }} className='border bg-yellow-500 text-white py-2 px-4 font-medium rounded hover:bg-yellow-700'>Thanh toán</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Fragment >
    )
}
