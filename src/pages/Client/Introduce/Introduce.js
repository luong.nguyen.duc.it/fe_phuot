import React, { useState } from 'react'
// import { ReactMapGL } from 'react-map-gl';
export default function Introduce() {
    const [viewport, setViewport] = useState({
        width: "100vw",
        height: "100vh",
        latitude: 21.0244246,
        longitude: 105.7938072,
        zoom: 16
    });
    let add = 1;

    const adr = (link) => {
        return (
            <div
                id="map-container-google-1"
                class="z-depth-1-half map-container"
                style={{
                    height: "500px",
                }}
            >
                <iframe
                    src={link}
                    frameborder="0"
                    style={{
                        border: "0",
                        height: "100%",
                        width: "100%",
                    }}
                    allowfullscreen
                ></iframe>
            </div>
        );
    };

    const renderMap = (add) => {
        switch (add) {
            case 1:
                return adr(
                    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29788.95788906226!2d105.80172724669009!3d21.047896052588197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abb158a2305d%3A0x5c357d21c785ea3d!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyDEkGnhu4duIEzhu7Fj!5e0!3m2!1svi!2s!4v1685807805856!5m2!1svi!2s");
            case 2:
                return adr(
                    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.757025313847!2d105.7995676761801!3d21.002374688682544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad5ac9beb0bd%3A0xc4069a08defd1deb!2sChung%20c%C6%B0%20Golden%20West!5e0!3m2!1svi!2s!4v1684310346651!5m2!1svi!2s"
                );
            default:
                return adr(
                    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.757025313847!2d105.7995676761801!3d21.002374688682544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad5ac9beb0bd%3A0xc4069a08defd1deb!2sChung%20c%C6%B0%20Golden%20West!5e0!3m2!1svi!2s!4v1684310346651!5m2!1svi!2s"
                );
        }
    };
    return (
        <div className='mt-52 grid grid-cols-12 mb-20'>
            <div className='col-start-3 col-span-8'>
                <div className='w-1/2'>
                    <img src='img/banner/slider3.png' alt='banner address' />
                </div>
                <div className='my-2 text-xl font-bold'>PHƯỢT BỤI STORE – ĐỒ PHƯỢT GIÁ TỐT</div>
                <div className='my-2'>Cửa hàng chuyển cung cấp các mặt hàng đồ phượt,đồ du lịch,đồ bảo hộ xe máy với hơn 5 năm kinh nghiệm trên thị trường shop Phượt Bụi Store luôn là sự lựa chọn hàng đầu đối với các phượt thủ chuyên nghiệp.</div>
                <div className='my-3'>
                    Cửa hàng bán buôn bán lẻ hơn 100 mặt hàng khác nhau, phân phối bán buôn bán lẻ toàn quốc uy tín và trách nhiệm
                </div>
                <div className='my-3'>
                    Địa chỉ: 235 Hoàng Quốc Việt, Cổ Nhuế, Bắc Từ Liêm, Hà Nội
                </div>
                <div className='my-3'>
                    Hotline: 0337074546 – 0972069410
                </div>
                <div>
                    {/* <ReactMapGL {...viewport} mapStyle="mapbox://styles/mapbox/streets-v11"
                        mapboxApiAccessToken="Your access token key!">
                    </ReactMapGL> */}
                    {renderMap(add)}
                </div>
            </div>
        </div>
    )
}
